FROM fluentd:latest

USER root
RUN apk add --no-cache --update tzdata geoip-dev libmaxminddb-dev
RUN apk add --no-cache --virtual .build-dependencies ruby-dev libc-dev gcc make shadow
RUN gem install fluent-plugin-elasticsearch fluent-plugin-rewrite-tag-filter fluent-plugin-concat fluent-plugin-filter-geoip2 --no-document
RUN useradd -g www-data -s /sbin/nologin www-data && groupmod -g 1000 www-data
RUN apk del --no-cache .build-dependencies
#COPY GeoIP /usr/share/GeoIP

USER www-data
ENTRYPOINT ["fluentd", "-c", "/fluentd/etc/fluent.conf"]
